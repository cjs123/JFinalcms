var CONFIG = require('../../utils/config.js')

Page({
  data: {
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    swipers: [],
    news: []
  },

  onLoad: function () {
    var that = this;
    wx.request({
      url: CONFIG.API_URL+"/index?siteId=1&categoryId=2",
      method: 'GET',
      data: {},
      header: {
        'Accept': 'application/json'
      },
      success: function(res) {
        console.log(res);

        if (res.statusCode == 200 && res.data.type == 'success') {
          that.setData({swipers: res.data.data.ads});
          that.setData({ news: res.data.data.contents});
        } else {
          
        }
      }
    })
  },
  onShareAppMessage: function () {
   // return custom share data when user share.
   console.log('onShareAppMessage')
   return {
      title: 'Cms',
      desc: '小程序',
      path: '/pages/index/index'
    }
  },
});
